import React, { Component } from "react";
import Review from "./Review";
import Rater from "./Rater";
import { Input, Button } from "antd";

const { TextArea } = Input;

export default class Reviews extends Component {
  state = {};

  render() {
    return (
      <div>
        <h4>Write a Review</h4>
        <Rater basic />
        <TextArea
          placeholder="How did you find this product?"
          style={{ marginTop: 16 }}
          rows={4}
        />
        <div
          style={{
            marginTop: 16,
            marginBottom: 16,
            display: "flex",
            justifyContent: "flex-end"
          }}
        >
          <Button type="primary" size="large">
            Submit
          </Button>
        </div>
        <Review />
        <Review />
        <Review />
        <Review />
        <Review />
        <Review />
      </div>
    );
  }
}
