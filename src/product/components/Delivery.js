import React, { Component } from "react";
import { Input } from "antd";

const Search = Input.Search;

export default class Delivery extends Component {
  state = {};

  render() {
    return (
      <div
        style={{
          border: "1px solid #ddd",
          borderRadius: 5,
          padding: 16,
          background: "#fafafa"
        }}
      >
        <h4>Delivery Details</h4>
        <Search
          placeholder="Enter Zip Code"
          enterButton="Estimate"
          value="400076"
          onSearch={value => console.log(value)}
        />
        <div style={{ color: "#444", marginTop: 16, fontSize: 14 }}>
          FREE shipping above <b>$150</b> to <b>400076</b>.
        </div>
        <div style={{ color: "#444", marginTop: 16, fontSize: 14 }}>
          Order within the next <b>17:29</b> minutes to receive by
          <b> Tue, Oct 31</b>.
        </div>
      </div>
    );
  }
}
