import React, { Component } from "react";
import { Row, Col, Button, Tabs } from "antd";
import Rater from "./Rater";
import Delivery from "./Delivery";
import Description from "./Description";
import Reviews from "./Reviews";

const TabPane = Tabs.TabPane;

export default class ProductDetails extends Component {
  state = {};

  render() {
    return (
      <div>
        <h2>Revolution Pro Blur Stick</h2>
        <Rater />
        <h1 style={{ marginTop: 16 }}>$99.00</h1>
        <Row>
          <Col span={14}>
            <Delivery />
          </Col>
        </Row>
        <div style={{ marginTop: 16, marginBottom: 32 }}>
          <Button
            type="primary"
            size="large"
            style={{ width: 180, marginRight: 16 }}
          >
            Buy Now
          </Button>
          <Button style={{ width: 180 }} size="large">
            Add To Cart
          </Button>
        </div>

        <Row style={{ marginRight: 16 }}>
          <Col span={24}>
            <Tabs defaultActiveKey="1" type="card">
              <TabPane tab="Description" key="1">
                <Description />
              </TabPane>
              <TabPane tab="Reviews" key="2">
                <Reviews />
              </TabPane>
            </Tabs>
          </Col>
        </Row>
      </div>
    );
  }
}
