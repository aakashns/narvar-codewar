import React from "react";
import { Breadcrumb } from "antd";

const Crumb = () => (
  <Breadcrumb style={{ margin: "16px 0" }} separator=">">
    <Breadcrumb.Item>Home</Breadcrumb.Item>
    <Breadcrumb.Item>Makeup</Breadcrumb.Item>
    <Breadcrumb.Item>Face</Breadcrumb.Item>
    <Breadcrumb.Item>Face Primer</Breadcrumb.Item>
  </Breadcrumb>
);

export default Crumb;
