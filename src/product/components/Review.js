import React, { Component } from "react";
import { Rate } from "antd";

export default class Review extends Component {
  state = {};

  render() {
    return (
      <div style={{ borderTop: "1px solid #ddd", paddingTop: 16 }}>
        <h4>Leaves a white residue.</h4>
        <span>
          <Rate value="3.7" /> | Rachael | Oct 9, 2018
        </span>
        <p style={{ marginTop: 12 }}>
          Does actually blur pores. Smooth and easy to apply but leave a whitish
          cast on my dusky skin, which makes it look like I've put too much
          makeup.
        </p>
      </div>
    );
  }
}
