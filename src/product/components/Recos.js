import React, { Component } from "react";
import Reco from "./Reco";

const recos = [
  {
    id: 1,
    img: "https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png",
    title: "Dior Destiny Handbag",
    description: "Become the new you."
  },
  {
    id: 2,
    img:
      "https://adn-static3.nykaa.com/media/catalog/product/tr:w-276,h-276,cm-pad_resize/l/i/lincon.jpg",
    title: "L'Oreal Paris Lips",
    description: "Better red than never."
  },
  {
    id: 3,
    img:
      "https://adn-static2.nykaa.com/media/catalog/product/tr:w-276,h-276,cm-pad_resize/6/9/6902395656258_1.jpg",
    title: "Matte Lips Choco",
    description: "Real chocolate extracts"
  }
];

export default class Recos extends Component {
  state = {};

  render() {
    return (
      <div>
        <h3 style={{ textAlign: "center" }}>Recommended for You</h3>
        {recos.map(d => (
          <Reco {...d} />
        ))}
      </div>
    );
  }
}
