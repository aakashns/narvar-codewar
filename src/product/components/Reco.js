import React, { Component } from "react";
import { Card } from "antd";

const { Meta } = Card;

export default class Reco extends Component {
  state = {};

  render() {
    const { img, title, description } = this.props;
    return (
      <div style={{ marginBottom: 16 }}>
        <Card hoverable cover={<img alt="example" src={img} />}>
          <Meta title={title} description={description} />
        </Card>
      </div>
    );
  }
}
