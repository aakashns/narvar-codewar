import React, { Component } from "react";

export default class Description extends Component {
  render() {
    return (
      <div>
        <div style={{ marginBottom: 16 }}>
          Revolution Pro Blur Stick works as a universal face primer that leaves
          skin perfectly smooth and matte. Apply before foundation or use alone
          as an invisible foundation to create the illusion of an even texture
          on bare skin.Suitable for all skin tones and types, this lightweight
          and breathable primer goes on clear.
        </div>
        <b>Features:</b>
        <ul>
          <li>Creates the perfect base for your foundation</li>
          <li>Controls oil and shine, skin looks mattified</li>
          <li>Minimizes the appearance of pores & fine lines</li>
          <li>Lightweight and breathable clear primer</li>
          <li>Suits all skin types and tones</li>
        </ul>
        <div style={{ marginTop: 16 }}>
          <b>About the Brand</b> : Introducing the Professional Makeup Range
          from the creators of Revolution Beauty.Aimed for professional makeup
          artists and beauty lovers worldwide, the new Pro range from Makeup
          Revolution London offers comprehensive range of Professional Makeup
          Products, Palettes, Eyebrow Pomade, Makeup Brushes, Lipsticks, along
          with face makeup products at affordable prices.While majority of the
          products are Vegan, these products are 100% cruelty free and free from
          animal testing.
        </div>
      </div>
    );
  }
}
