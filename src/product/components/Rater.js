import React, { Component } from "react";
import { Rate } from "antd";

export default class Rater extends Component {
  state = {
    value: 4.1
  };

  handleChange = value => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;
    const { basic } = this.props;
    return (
      <span>
        <Rate onChange={this.handleChange} value={value} allowHalf />
        {value && (
          <span className="ant-rate-text">
            {value} stars {basic ? "" : "(3.7k ratings, 197 reviews)"}
          </span>
        )}
      </span>
    );
  }
}
