import React, { Component } from "react";
import { Carousel, Row, Col } from "antd";

import "./ProductImages.css";

const SAMPLE_IMG = [
  "https://adn-static1.nykaa.com/media/catalog/product/tr:w-344,h-344,cm-pad_resize/5/0/5057566032384a.jpg",
  "https://adn-static1.nykaa.com/media/catalog/product/tr:w-344,h-344,cm-pad_resize/5/0/5057566032384b.jpg",
  "https://adn-static2.nykaa.com/media/catalog/product/tr:w-344,h-344,cm-pad_resize/5/0/5057566032384.jpg",
  "https://adn-static3.nykaa.com/media/catalog/product/tr:w-344,h-344,cm-pad_resize/3/5/3548752082730_pack_1.jpg"
];

export default class ProductImages extends Component {
  state = {
    current: 0
  };

  render() {
    return (
      <div>
        <Carousel afterChange={() => {}}>
          <div>
            <img
              src={SAMPLE_IMG[0]}
              style={{ height: "100%", width: "100%" }}
              alt="sample"
            />
          </div>
          <div>
            <img
              src={SAMPLE_IMG[1]}
              style={{ height: "100%", width: "100%" }}
              alt="sample"
            />
          </div>
          <div>
            <img
              src={SAMPLE_IMG[2]}
              style={{ height: "100%", width: "100%" }}
              alt="sample"
            />
          </div>
          <div>
            <img
              src={SAMPLE_IMG}
              style={{ height: "100%", width: "100%" }}
              alt="sample"
            />
          </div>
        </Carousel>
        <Row>
          {SAMPLE_IMG.map((img, i) => (
            <Col key={i} span={6}>
              <img src={img} style={{ height: 72, width: 72 }} alt="sample" />
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}
