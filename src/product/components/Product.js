import React, { Component } from "react";
import { Row, Col } from "antd";
import ProductImages from "./ProductImages";
import ProductDetails from "./ProductDetails";
import Recos from "./Recos";

export default class Product extends Component {
  state = {};

  render() {
    return (
      <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
        <Row>
          <Col span={8}>
            <ProductImages />
          </Col>
          <Col span={12}>
            <ProductDetails />
          </Col>
          <Col span={4}>
            <Recos />
          </Col>
        </Row>
      </div>
    );
  }
}
