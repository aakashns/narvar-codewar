import React, { Component } from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import Crumb from "../components/Crumb";
import Product from "../components/Product";

import "./ProductPage.css";

const { Header, Content, Footer } = Layout;

export default class ProductPage extends Component {
  render() {
    return (
      <Layout className="layout" style={{ minHeight: "100vh" }}>
        <Header>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["1"]}
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item key="1">
              <b>SuperLux Online</b>
            </Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <Crumb />
          <Product />
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Created by: Aakash N S ©2018 SuperLux Online
        </Footer>
      </Layout>
    );
  }
}
