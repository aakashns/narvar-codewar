import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import ProductPage from "./product/pages/ProductPage";

import "antd/dist/antd.css";
import "./index.css";

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" component={ProductPage} />
    </Switch>
  </BrowserRouter>
);

ReactDOM.render(<App />, document.getElementById("root"));
