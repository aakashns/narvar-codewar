# ECommerce Product Details Page

Made for Narvar Code War

## Live Demo

https://narvar-codewar.firebaseapp.com/

## Screenshots

Please see the screenshots folder

![Photo 1](./screenshots/screen1.png)

![Photo 2](./screenshots/screen2.png)

![Photo 3](./screenshots/screen3.png)

![Photo 4](./screenshots/screen4.png)
